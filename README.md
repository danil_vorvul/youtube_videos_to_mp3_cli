# youtube_videos_to_mp3_cli

With this script you can download youtube video or all videos from playlist and convert them to mp3.

## Help
Options:
| Args             | Type | Description                           | Additional                  |
|------------------|------|---------------------------------------|-----------------------------|
| -u, --url        | TEXT | Url to youtube video/playlist         | [required]                  |
| -o, --out-folder | TEXT | Path to folder where to store mp3.    | [default=HOME/Music]        |
| --dont-overwrite | BOOL | Overwrite download folder with videos | [for testing, deafult=True] |
| --help           | FLAG | Show this message and exit.           |                             | 
## How to
Install needle dependecies
```bash
    pip3 install -r requirements.txt
```
Now you can download video or playlist via link
```bash
    python3 transfer.py -u https://www.youtube.com/watch?v=h3EEhWecuoA
```

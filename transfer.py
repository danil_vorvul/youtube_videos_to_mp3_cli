import os
import click
import youtube_dl
import moviepy.editor as mp


class BCOLORS:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"


def setup(dont_overwrite):
    download_folder = "videos"
    ytdl_options = {
        # TODO maybe add some functionality with download options
        #'format': 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4',
        #    "outtmpl": "videos/%(title)s.%(ext)s",
        #    "restrictfilenames": True,
        #    "cachedir": False,
        #    "playlist": True,
        "format": "bestaudio[ext=mp4]/mp4",
        "outtmpl": "videos/%(title)s.%(ext)s",
    }

    if not dont_overwrite:
        # clean folder
        os.popen(f"rm -rf {download_folder}")

    return (
        download_folder,  # folder where we want to download
        ytdl_options,  # download options
    )


@click.command()
@click.option(
    "--url",
    "-u",
    "url",
    required=True,
    help="Url to youtube video/playlist.",
)
@click.option(
    "--out-folder",
    "-o",
    default=f"{os.environ['HOME']}/Music",
    help="Path to folder where to store mp3.",
)
@click.option(
    "--dont-overwrite",
    "dont_overwrite",
    help="Overwrite download folder with videos (for testing)",
    flag_value=True,
    default=False,
)
def process(url, out_folder, dont_overwrite=False):
    """main

    Args:
        in_file ([type]): [description]
        out_file ([type]): [description]
    """
    print(BCOLORS.HEADER + "Start working ..." + BCOLORS.ENDC)

    download_folder, ytdl_options = setup(dont_overwrite)

    print(
        BCOLORS.HEADER
        + "Setup was successfull, Start video downloading ..."
        + BCOLORS.ENDC
    )
    with youtube_dl.YoutubeDL(ytdl_options) as ydl:
        ydl.download([url])

    files = os.listdir(f"{download_folder}/")
    videos = [
        {"filename": f, "title": title[0]}
        for f in files
        if (title := os.path.splitext(f))[1] == ".mp4"
    ]

    # create output folder
    if not os.path.exists(out_folder):
        os.mkdir(out_folder)

    print(
        BCOLORS.HEADER
        + "All videos was downloaded. Start video to audioconverting ..."
        + BCOLORS.ENDC
    )
    for video in videos:
        # Insert Local Video File Path
        clip = mp.VideoFileClip("/".join([download_folder, video["filename"]]))
        # Insert Local Audio File Path
        clip.audio.write_audiofile(
            "/".join([out_folder, ".".join([video["title"], "mp3"])])
        )

    print(
        BCOLORS.OKGREEN
        + f"Your videos was downloaded and converted to mp3 to {out_folder}"
        + BCOLORS.ENDC
    )


process()
